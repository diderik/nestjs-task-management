import { Injectable, NotFoundException } from '@nestjs/common';
import { Task, TaskStatus } from './task.model';
import { v4 as uuid} from 'uuid';
import { CreateTaskDto } from './dto/create-task.dto';
import { GetTaskFilterDto } from './dto/get-tasks-filter.dto';

@Injectable()
export class TasksService {
    private tasks: Task[] = [];

    getAllTasks(): Task[] {
        return this.tasks;

    }

    getTasksWithFilters(filterDto: GetTaskFilterDto): Task[] {
        const {status, search} = filterDto;

        let tasks = this.getAllTasks();

        if (status) {
            tasks = tasks.filter(task => task.status === status);
        }

        if (search) {
            tasks = tasks.filter(task =>
                task.title.includes(search) ||
                task.description.includes(search),);
        }
        return tasks;
    }

    getTaskById(id: string): Task {
        const found = this.tasks.find(task => task.id === id);

        if (!found) {
            throw new NotFoundException("Task with ID "+id+" not found.");
        }

        return found;
    }

    createTask(createTaskDto: CreateTaskDto): Task {
        // Trick to create and assign 2 variables
        const { title, description } = createTaskDto;

        // Create new task object
        const task: Task = {
            id: uuid(),
            title, // shorthand for title: title
            description,
            status: TaskStatus.OPEN
        };

        // Add created task object to private array
        this.tasks.push(task);
        // return created task object to caller
        return task;
    }

    updateTaskStatus(id: string, status: TaskStatus): Task {
        //Find task and if found, update status
        // Cast status string to enum
        const task = this.getTaskById(id);
        task.status = status;
        return task;

        /* My try at the challenge (which works if correct status string given, otherwise the status disappears)
        const enumStatus : TaskStatus = TaskStatus[status];
        let task: Task;

        this.tasks.forEach((element, index) => {
            if(element.id === id) {
                task = this.tasks[index];
                task.status = enumStatus;
            }
        });
        return task;
        */
    };

    deleteTask(id: string): void {
        //Remove an item from the array by creating a new array without the item to remove
        const found = this.getTaskById(id);
        
        this.tasks = this.tasks.filter(task => task.id !== found.id);
    }
}
